from ezcurses import Cursed

with Cursed() as scr:
    msgs = [
        ('a', 'Add New Item'),
        ('c', 'Copy something'),
        ('f', 'Find Thing'),
        (None, 'No Key for This'),
        ('q', 'Quit'),
    ]
    win = scr.new_win(orig=(0, 0), size=scr.max_size())
    item = win.get_menu_item(msgs)

print(item)
