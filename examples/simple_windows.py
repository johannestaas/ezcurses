from ezcurses import Cursed

with Cursed() as scr:
    w, h = scr.max_size()

    win1 = scr.new_win(orig=(0, 0), size=(40, 40))
    win2 = scr.new_win(orig=(40, 0), size=(40, 40))

    win1.border()
    win2.border()

    win2.background('.', color=('green', 'blue'))

    win1.refresh()
    win2.refresh()

    s = win1.getstr((1, 1), echo=True)

    win2.hline('-', (0, 19))
    win2.change_color(pos=(0, 19), n=40, color=('black', 'red'))

    win2.hline('=', (0, 21))
    win2.change_color(pos=(0, 21), n=40, color=('black', 'red'))

    win2.write(s, (20 - (len(s) // 2), 20), color=('red', 'black'))
    win2.refresh()

    win1.write('Press q to quit', (1, 1), color=('black', 'red'))
    while win1.getkey() != 'q':
        pass
