from ezcurses import curse


@curse
def main(scr):
    layout = scr.new_layout()
    layout.add_row(3, [4, 4, 4])
    layout.add_row(9, [6, 6])
    layout.draw()
    for row in layout:
        for col in row:
            col.write('Hey there!', pos=(0, 0), color=('blue', 'white'))
            col.refresh()
            scr.getch()


main()
