from time import sleep
from ezcurses import curse

@curse
def main(scr, message_string):
    w, h = scr.max_size()
    scr.write(message_string, pos=(w // 2, h // 2))
    scr.refresh()
    sleep(1)

if __name__ == '__main__':
    main('Hello world!')
