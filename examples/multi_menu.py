from ezcurses import Cursed

with Cursed() as scr:
    msgs = [
        ('m', 'Mocha'),
        ('c', 'Coffee'),
        ('t', 'Tea'),
        ('l', 'Latte'),
    ]
    msgs2 = [
        ('s', 'small'),
        ('m', 'medium'),
        ('l', 'large'),
    ]
    msgs3 = [
        ('0', 'no sugar'),
        ('1', '1 spoon of sugar'),
        ('2', '2 spoons of sugar'),
        ('9', '9 spoons of sugar'),
    ]
    msgs4 = [
        ('0', 'no milk'),
        ('1', 'skim milk'),
        ('2', 'whole milk'),
        ('3', 'cream'),
        ('4', 'heavy cream'),
    ]
    win = scr.new_win(orig=(0, 0), size=scr.max_size())
    menu = win.new_menu(msgs, selected=1)
    menu2 = win.new_menu(msgs2, orig=(20, 0), selected=1)
    menu3 = win.new_menu(msgs3, orig=(0, 6), selected=0)
    menu4 = win.new_menu(msgs4, orig=(20, 6), selected=3)
    results = win.multi_menu([menu, menu2, menu3, menu4])


print(results)
print('selected {!r} drink'.format(msgs[results[0]][1]))
print('selected {!r} size'.format(msgs2[results[1]][1]))
print('selected {!r} amount of sugar'.format(msgs3[results[2]][1]))
print('selected {!r} type dairy'.format(msgs4[results[3]][1]))
