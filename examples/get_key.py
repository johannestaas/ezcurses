from ezcurses import curse


@curse
def main(scr):
    w, h = scr.max_size()
    scr.background(' ', color=('blue', 'white'))
    while True:
        scr.write('Press "q" to quit!', (0, h - 1), color=('red', 'black'))
        key = scr.getkey()
        msg = 'You pressed: {!r}'.format(key)
        half_len = len(msg) // 2
        pos = (w // 2 - half_len, h // 2)
        scr.write(msg, pos, color='red')
        scr.refresh()
        if key == 'q':
            return


main()
