.. ezcurses documentation master file, created by
   sphinx-quickstart on Mon Jun 19 20:38:28 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ezcurses Screen
===============

.. automodule:: ezcurses.screen
    :members:
    :undoc-members:
    :show-inheritance:
 

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

